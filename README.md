# nodeJs-realTimeChat

This is my first big project with NodeJs.

Currently the implemented (and fully functional) features are
 * broadcast to every connected user
 * send message to selected user
 * Telegram login (does not affect anything)

You can probably check out the working instance at https://startui.ddns.net (unless it is offline at the moment).
